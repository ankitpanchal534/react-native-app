import { Home, Profile } from "../Screens";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MainRoutes from "./MainRoutes";

export default function StackNavigation() {
    const Stack = createNativeStackNavigator();
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                {MainRoutes(Stack)}
            </Stack.Navigator>
        </NavigationContainer>
    )
}