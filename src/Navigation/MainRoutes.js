import { View, Text } from 'react-native'
import React from 'react'
import BottomNavigation from './BottomNavigation'

export default function (Stack) {
    return (
        <>
            <Stack.Screen
                name="Tabs"
                component={BottomNavigation}
            />
        </>
    )
}