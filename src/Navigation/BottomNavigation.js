import { View, Text, Button } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Home, Profile, Explore } from "../Screens";
import Entypo from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign'
export default function BottomNavigation() {

    const Tab = createBottomTabNavigator()
    return (
        <Tab.Navigator
            screenOptions={{
                presentation: 'modal',
                headerShown: false,
                tabBarStyle: {
                    padding: 5,
                    borderRadius: 35,
                    // borderTopStartRadius: 35,
                    // borderTopEndRadius: 35,
                    bottom: 5,
                    marginHorizontal: 10
                    // backgroundColor: 'white'
                },
                // tabBarShowLabel: false

            }}

            initialRouteName="Home">
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (<Entypo name='home' style={{ color: focused ? 'purple' : "gray" }} size={27} />)
                    }
                }}
            />

            <Tab.Screen
                name="Explore"
                component={Explore}
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (<AntDesign name='appstore1' style={{ color: focused ? 'purple' : "gray" }} size={27} />)
                    }
                }}
            />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (<Entypo name='user' size={27} style={{ color: focused ? 'purple' : "gray" }} />)
                    }
                }}
            />
        </Tab.Navigator>
    )
}