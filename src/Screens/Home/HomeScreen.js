import { View, Text, Button } from 'react-native'
import React from 'react'
import Entypo from 'react-native-vector-icons/Entypo'

export default function HomeScreen({ navigation }) {
    console.log("props", { navigation })
    return (
        <View>
            <Text>This is HomeScreen</Text>
            <Entypo name="home" size={20} color="black" />
        </View>
    )
}